from sense_hat import SenseHat
from time import *

s = SenseHat()
s.low_light = True

green = (0, 255, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (200, 0, 0)
white = (255,255,255)
nothing = (0,0,0)
pink = (255,105, 180)
black = (0, 0, 0)

def sensor():
    sense = SenseHat()
    #read in the sensor values as number variables
    temp = round(sense.temp,1)
    pressure = round(sense.pressure,1)
    humidity = round(sense.humidity,1)

    #conditionally set the colour of the temperature output
    if temp > 10 and temp < 25:
        tempcolour = green
    elif temp >= -10 and temp <=10:
        tempcolour = yellow
    else:
        tempcolour = red

    #conditionally set the colour of the pressure output
    if pressure > 900:
        pressurecolour = green
    elif pressure >= 500 and pressure <=900:
        pressurecolour = yellow
    else:
        pressurecolour = red

    
    #conditionally set the colour of the humidity output
    if humidity >= 25 and humidity <= 60:
        humiditycolour = green
    elif humidity > 60:
        humiditycolour = red
    else:
        humiditycolour = yellow

    #convert numbers to strings for display
    temp = str(temp)
    pressure = str(pressure)
    humidity = str(humidity)

    #show temperature
    sense.show_letter("T")
    sleep(0.5)
    sense.show_message(temp, text_colour=tempcolour)

    #show pressure
    sense.show_letter("P")
    sleep(0.5)
    sense.show_message(pressure, text_colour=pressurecolour)

    #show humidity
    sense.show_letter("H")
    sleep(0.5)
    sense.show_message(humidity, text_colour=humiditycolour)

def screen():

  def raspi_logo():
      G = green
      R = red
      O = nothing
      logo = [
      O, G, G, O, O, G, G, O, 
      O, O, G, G, G, G, O, O,
      O, O, R, R, R, R, O, O, 
      O, R, R, R, R, R, R, O,
      R, R, R, R, R, R, R, R,
      R, R, R, R, R, R, R, R,
      O, R, R, R, R, R, R, O,
      O, O, R, R, R, R, O, O,
      ]
      return logo
  
  images = [raspi_logo]
  count = 0
  while True:
    speed = 0.05    #set the speed variable to control the scrolling speed. The higher the number, the slower the scrolling
    message = "Space Camp"     #set this variable to a string of your choosing
    s.show_message(message, speed, text_colour=green, back_colour=black) #show the message with the settings defined in the variables above
    s.set_pixels(images[count % len(images)]())
    sleep(0.75)
    count += 1
    sensor()
    
screen()